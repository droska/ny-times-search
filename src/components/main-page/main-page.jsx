import React, { Component } from "react";
import SearchBar from "../search-bar/search-bar";
import axios from "axios";

class MainPage extends Component {
    state = {};

    async onSearchSubmit(keywords, types) {
        const keywords_query = keywords.replace(" ", "%20");
        const response = await axios.get(
            "https://api.nytimes.com/svc/search/v2/articlesearch.json?q=" +
                keywords_query +
                "&fq=type_of_material:%22" +
                types +
                "%22&page=0&api-key=OPBbNyKzxILXX8iqvtFdROG5WK80qLuY"
        );
        console.log(response.data.response.docs);
        sessionStorage.setItem(
            "response",
            JSON.stringify(response.data.response.docs)
        );
        sessionStorage.setItem("keywords_query", keywords_query);
        sessionStorage.setItem("types", types);
        window.location.href = "/results-page";
    }

    render() {
        return (
            <div className="main-page">
                <SearchBar onSubmit={this.onSearchSubmit} />
            </div>
        );
    }
}

export default MainPage;
