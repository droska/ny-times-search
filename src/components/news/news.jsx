import React, { Component } from "react";
import "./news.scss";

class News extends Component {
    state = {};

    render() {
        var getList = sessionStorage.getItem("response");
        var response = JSON.parse(getList);

        const renderData = response.map((item, index) => (
            <div className="ui grid" style={{ marginBottom: "20px" }}>
                <div className="three wide column thumb">
                    {typeof item.multimedia[0] !== "undefined" ? (
                        <img
                            src={
                                "https://www.nytimes.com/" +
                                item.multimedia[0].legacy.xlarge
                            }
                            alt="thumbnail"
                        />
                    ) : (
                        ""
                    )}
                </div>
                <div
                    className={
                        typeof item.multimedia[0] !== "undefined"
                            ? "thirteen wide column article"
                            : "sixteen wide column article"
                    }
                >
                    <a href={item.web_url} key={index}>
                        {item.snippet}
                    </a>
                    <p className="lead-paragraph">{item.lead_paragraph}</p>
                    <p>
                        <span style={{ fontWeight: "bold" }}>Source: </span>{" "}
                        {item.source}
                    </p>
                    <p>
                        <span style={{ fontWeight: "bold" }}>Published: </span>{" "}
                        {item.web_url.match(/\d{4}(?:\/\d{2})*/)}
                    </p>
                    {typeof item.keywords[0] !== "undefined" ? (
                        <p>
                            <span className="tags">
                                <i className="fas fa-tags" />
                            </span>
                            {item.keywords[0].value}
                        </p>
                    ) : (
                        ""
                    )}
                </div>
            </div>
        ));

        return <div className="news-wrapper">{renderData}</div>;
    }
}

export default News;
