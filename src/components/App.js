import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import "./App.scss";
import MainPage from "./main-page/main-page";
import Results from "./results-page/results";

class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <div>
                        <Route path="/" exact component={MainPage} />
                        <Route path="/results-page" component={Results} />
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
